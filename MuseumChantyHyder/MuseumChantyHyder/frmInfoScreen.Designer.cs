﻿namespace MuseumChantyHyder
{
    partial class frmInfoScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNaamKunst = new System.Windows.Forms.Label();
            this.lblID = new System.Windows.Forms.Label();
            this.lblKunstenaar = new System.Windows.Forms.Label();
            this.lblInfo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblNaamKunst
            // 
            this.lblNaamKunst.AutoSize = true;
            this.lblNaamKunst.Location = new System.Drawing.Point(33, 56);
            this.lblNaamKunst.Name = "lblNaamKunst";
            this.lblNaamKunst.Size = new System.Drawing.Size(35, 13);
            this.lblNaamKunst.TabIndex = 0;
            this.lblNaamKunst.Text = "Naam";
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Location = new System.Drawing.Point(33, 31);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(68, 13);
            this.lblID.TabIndex = 1;
            this.lblID.Text = "KunstwerkID";
            this.lblID.Click += new System.EventHandler(this.label2_Click);
            // 
            // lblKunstenaar
            // 
            this.lblKunstenaar.AutoSize = true;
            this.lblKunstenaar.Location = new System.Drawing.Point(33, 80);
            this.lblKunstenaar.Name = "lblKunstenaar";
            this.lblKunstenaar.Size = new System.Drawing.Size(61, 13);
            this.lblKunstenaar.TabIndex = 2;
            this.lblKunstenaar.Text = "Kunstenaar";
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Location = new System.Drawing.Point(33, 117);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(49, 13);
            this.lblInfo.TabIndex = 3;
            this.lblInfo.Text = "ExtraInfo";
            // 
            // InfoScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(418, 400);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.lblKunstenaar);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.lblNaamKunst);
            this.Name = "InfoScreen";
            this.Text = "InfoScreen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNaamKunst;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Label lblKunstenaar;
        private System.Windows.Forms.Label lblInfo;
    }
}