﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuseumChantyHyder
{
    class Archief
    {
        private Kunstwerk[] Kunstwerken = new Kunstwerk[6];
        private Kunstenaar[] Kunstenaars = new Kunstenaar[4];
        private string[] namen = new string[3];

        private Kunstenaar Kunstenaar1;
        private Kunstenaar Kunstenaar2;
        private Kunstenaar Kunstenaar3;
        private Kunstenaar Kunstenaar4;
        
        private Kunstwerk kunstwerk1;
        private Kunstwerk kunstwerk2;
        private Kunstwerk kunstwerk3;
        private Kunstwerk kunstwerk4;
        private Kunstwerk kunstwerk5;
        private Kunstwerk kunstwerk6;

        public Archief()
        {
            Kunstenaar1 = new Kunstenaar("Andy Warhol", 1928);
            Kunstenaar2 = new Kunstenaar("Leonarde Da Vinci", 1452);
            Kunstenaar3 = new Kunstenaar("Jean-Michel Basquiat", 1960);
            Kunstenaar4 = new Kunstenaar("Terry Richardson", 1965);

            kunstwerk1 = new Kunstwerk(1, "Hollywood Africans", "Het gaat hier om een kunstwerk in de stijl neo-expressionisme.", Kunstenaar3);
            kunstwerk2 = new Kunstwerk(2, "Mona Lisa","Een van 's werelds bekendste schilderijen.", Kunstenaar2);
            kunstwerk3 = new Kunstwerk(3, "Het laatste avondmaal", "Het is een afbeelding van een scène uit het Laatste Avondmaal van Jezus zoals beschreven in de Bijbel en het is gebaseerd op Johannes 13:21-26 waarin Jezus aankondigt dat één van zijn twaalf discipelen hem zal verraden. Het schilderij is wereldwijd bekend en de opstelling is een iconografisch model. Omdat het niet kan worden verplaatst is het nooit in particulier bezit geweest.", Kunstenaar2);
            kunstwerk4 = new Kunstwerk(4, "Campbell's Soup Cans", "It consists of thirty-two canvases, each measuring 20 inches (51 cm) in height × 16 inches (41 cm) in width and each consisting of a painting of a Campbell's Soup can—one of each of the canned soup varieties the company offered at the time.[1] The individual paintings were produced by a printmaking method—the semi-mechanized screen printing process, using a non-painterly style. ", Kunstenaar1);
            kunstwerk5 = new Kunstwerk(5, "Rorschach", "This painting belongs to a series modeled on the famous inkblot test invented by the Swiss psychiatrist Hermann Rorschach. Whereas the actual test provides ten standardized blots for a patient to decipher, Warhol invented his own, achieved by painting one side of a canvas and then folding it vertically to imprint the other half. Ironically, Warhol originally misinterpreted the clinical process, believing that patients created the inkblots and doctors interpreted them: “I thought that when you went to places like hospitals, they tell you to draw and make the Rorschach Tests. I wish I’d known there was a set.” Because of this misunderstanding, Warhol’s Rorschach series is one of the few in which the artist does not rely on preexisting images.", Kunstenaar1);
            kunstwerk6 = new Kunstwerk(6, "The Face","Een van zijn bekendste werken is de foto van The Face, een rapgroep uit de jaren 90", Kunstenaar4);

            Kunstwerken[0] = kunstwerk1;
            Kunstwerken[1] = kunstwerk2;
            Kunstwerken[2] = kunstwerk3;
            Kunstwerken[3] = kunstwerk4;
            Kunstwerken[4] = kunstwerk5;
            Kunstwerken[5] = kunstwerk6;
            Kunstenaars[0] = Kunstenaar1;
            Kunstenaars[1] = Kunstenaar2;
            Kunstenaars[2] = Kunstenaar3;
            Kunstenaars[3] = Kunstenaar4;
        }

        public int GetKunstwerk(Kunstwerk kunstwerk)
        {
            return kunstwerk.GetKunstId;
        }


    }
}
