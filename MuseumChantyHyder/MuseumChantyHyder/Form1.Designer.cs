﻿namespace MuseumChantyHyder
{
    partial class frmStartScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblKunstID = new System.Windows.Forms.Label();
            this.txtKunstID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cboKunstenaar = new System.Windows.Forms.ComboBox();
            this.btnOntdek = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(34, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(324, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Museum van Schone Kunsten";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(35, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Audiogids";
            // 
            // lblKunstID
            // 
            this.lblKunstID.AutoSize = true;
            this.lblKunstID.Location = new System.Drawing.Point(98, 128);
            this.lblKunstID.Name = "lblKunstID";
            this.lblKunstID.Size = new System.Drawing.Size(147, 13);
            this.lblKunstID.TabIndex = 2;
            this.lblKunstID.Text = "Vul hier de kunstwerkcode in:";
            this.lblKunstID.Click += new System.EventHandler(this.lblKunstID_Click);
            // 
            // txtKunstID
            // 
            this.txtKunstID.Location = new System.Drawing.Point(126, 162);
            this.txtKunstID.Name = "txtKunstID";
            this.txtKunstID.Size = new System.Drawing.Size(100, 20);
            this.txtKunstID.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(86, 207);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(182, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Of selecteer een van de kunstenaars";
            // 
            // cboKunstenaar
            // 
            this.cboKunstenaar.FormattingEnabled = true;
            this.cboKunstenaar.Items.AddRange(new object[] {
            "Andy Warhol",
            "Leonardo Da Vinci",
            "Jean-Michel Basquiat",
            "Terry Richardson"});
            this.cboKunstenaar.Location = new System.Drawing.Point(115, 242);
            this.cboKunstenaar.Name = "cboKunstenaar";
            this.cboKunstenaar.Size = new System.Drawing.Size(121, 21);
            this.cboKunstenaar.TabIndex = 5;
            // 
            // btnOntdek
            // 
            this.btnOntdek.Location = new System.Drawing.Point(140, 291);
            this.btnOntdek.Name = "btnOntdek";
            this.btnOntdek.Size = new System.Drawing.Size(75, 23);
            this.btnOntdek.TabIndex = 6;
            this.btnOntdek.Text = "ONTDEK";
            this.btnOntdek.UseVisualStyleBackColor = true;
            this.btnOntdek.Click += new System.EventHandler(this.btnOntdek_Click);
            // 
            // frmStartScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 397);
            this.Controls.Add(this.btnOntdek);
            this.Controls.Add(this.cboKunstenaar);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtKunstID);
            this.Controls.Add(this.lblKunstID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmStartScreen";
            this.Text = "StartScreen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblKunstID;
        private System.Windows.Forms.TextBox txtKunstID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboKunstenaar;
        private System.Windows.Forms.Button btnOntdek;
    }
}

