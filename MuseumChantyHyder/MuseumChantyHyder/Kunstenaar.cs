﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuseumChantyHyder
{
    class Kunstenaar
    {
        private string NaamKunstenaar;
        private int Geboortejaar;

        public Kunstenaar(string name, int date)
        {
            NaamKunstenaar = name;
            Geboortejaar = date;
        }

        public string GetNaam
        {
            get{
                return NaamKunstenaar;
            }
        }

        public int GetGeboortejaar
        {
            get{
                return Geboortejaar;
            }
        }
    }
}
