﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuseumChantyHyder
{
    class Kunstwerk
    {
        private int KunstwerkID;
        private string NaamKunstwerk;
        private string InfoKunstwerk;
        private Kunstenaar Kunstenaar;

        public Kunstwerk(int ID, string NaamKunst, string InfoKunst, Kunstenaar kunstenaar)
        {
            KunstwerkID = ID;
            NaamKunstwerk = NaamKunst;
            InfoKunstwerk = InfoKunst;
            Kunstenaar = kunstenaar;
        }

        public string GetInfo
        {
            get{return InfoKunstwerk;}
        }

        public string GetNaamKunstwerk
        {
            get{return NaamKunstwerk;}
        }

        public Kunstenaar GetKunstenaar
        {
            get{return Kunstenaar;}
        }

        public int GetKunstId
        {
            get{return KunstwerkID;}
        }
    }
}
